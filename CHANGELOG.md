# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- `transfer.sh` alias now creates qr code if `qrencode` is installed
- Added offline msmtp email queuing
- Added sending in background to `neomutt`
- Created systemd service for compton
- Added `i3blocks` configuration
- Added synctex support to neovim and zathura
- Added Markdown bibliography completion to `neovim`
- Added LaTeX autocompletion to `neovim`
- Added `Ctrl-Space` binding in `ranger` to open new terminal at directory
- Added `dmenu` script to choose a wallpaper
- Added `unclutter` to hide mouse on no activity
- Added time to zsh prompt
- `termite` configuration
- Added nerd icons to ranger

### Changed

- Prune folders from `dmenumount` that already have mounts in them
- Removed `polybar` from installation, moved to `i3blocks`
- Moved all applications to using the monospace definition in the fonts configuration
- Changed some gpg settings
- Moved from colorscheme plugins to my own files
- Moved from `antigen` to `antibody` as `zsh` backend for hopefully more stable
- Moved to `dmenu_run` for launching applications
- Made `dmenucolor` script more universal
- Changed up the fonts used a bit
- Moved mail/calendar/address book password management to `pass`
- Moved from `crontab` to `systemd` service timer for offlineimap and vdirsyncer
- Moved from `urxvt` to `termite` for true color support and has all the
  features I hacked into `urxvt`
- Default search engine is now DuckDuckGo
- Increase lock speed by 6x, now instantaneous
- Updated `compton` configuration
- Moved from `vim` installed `fzf` to a global installation
- Moved environmental variables to the correct place

## [0.5.0] - 2019-01-24

### Added

- `ueberzug` for ranger image preview
- added wallpaper collection
- `displayselect` script for a `dmenu` based display selection
- `pdfpcnotes.sty` to generate pdfpc notes from latex beamer
- `vifm` configuration
- systemd service to start `dunst` with the correct colors makes it easy to
  check status and ensure it is running
- created `sxiveh` script to view remote images/gifs in `sxiv`
- `sxiv` application to replace `feh`
- `G'MIC` plugin to Gimp
- `entr` and auto compilation to vim
- vim mapping to open html file in browser
- `compiler` script to handle all compilation options

### Changed

- moved from `pywal` to `wpgtk`
- moved `md` `pandoc` compilation back to LaTeX from `groff`
- script reorganization and complete overhaul
- made `qutebrowser` the `$TRUEBROWSER` and use `linkhandler` as the `$BROWSER`
- moved `i3` variables to environment variables
- moved LaTeX style files to where they are automatically detected
- `zathura` opens not recolored by default
- rewrote how `autocomp` is used in `vim`
- fixed a bug with vim autocompiler
- moved default file browser to `vifm`
- moved wallpaper manager to `xwallpaper` and fully got rid of `feh`
- moved rtv from `feh` to `sxiv`
- moved from `scrot` to `maim` for screenshots

## [0.4.0] - 2018-11-22

### Added

- bluetooth support
- `transfer.sh` alias for easy file sharing from the command line
- vim bindings to complete glossary entries
- rmarkdown beamer templates and aliases
- global `.latexmkrc` file to do global glossary handling
- `$XDG_CONFIG_HOME` variable
- dmenu script to easily insert emojis (bound to alt-e in i3)
- Symbola emoji font
- pretty emails from mutt converting them to HTML using pandoc
- `sane` and `gscan2pdf` packages for scanner support
- Wikipedia search engine in qutebrowser
- `lmount` and `lumount` scripts to easily mount and unmount encrypted drives
- Added `pamusb` application to use usb device as smartkey
- Added ability to bookmarks binding to act as qutebrowser url bar when
  something is typed that isn't a bookmark
- Added alternate action ability to `dmenuexpand` script
- `limitlog` script to pipe to a log file that has a max size and removes the
  first `n` lines when it gets too big
- Polybar module to display btrfs snapshot backup script
- Moved from `pure` zsh prompt to `spaceship` zsh prompt
- Added ipc messaging to polybar for module updating instead of interval
  checking
- Removed screen tearing with compton
- Added `rsync`, `openssh`, and `xord-xrandr` to installation
- Created simple stopwatch script

### Changed

- R markdown quiet compilation
- Added `pandoc-citeproc` filter for bibliography support for `md` compilation
- Moved markdown compilation to roff instead of LaTeX to see how it is
- Added rmarkdown support to pdf presentation binding
- Added kill line to `remaps` script to keep xcape only running once
- Moved some dotfiles out of the home directory and to `$XDG_CONFIG_HOME`
- Fixed wal options to work with the latest version
- Fixed physical key unlock with non-forking lock script to disable dunst
- Fixed dunst notifications over the lockscreen
- Moved `khal` and `khard` to pip install to fix some specific dependency issues
- Updated `rtv` package name in aur

## [0.3.0] - 2018-08-07

### Added

- `qutebrowser` bindings in `shortcuts` script to download to a folder
- Increase key speed in remaps
- Natural resizing to `i3`
- `tmuxinator` to Makefile
- `gpmm` - git pull and merge master into branch alias
- Power menu dmenu script bound to `alt-ctrl-shift-page down`
- `pushd` and `popd` aliases
- Custom sudoers privileges support
- Added mount and umount dmenu scripts bound to mod-z and mod-shift-z
  respectively
- Added dmenu wrapper script to add color and font along with aliasing dmenu to
  dmenucolor

### Changed

- Updated shortcuts script to be more efficient
- Full github urls to antigen to be more explicit
- Updated to `ncm2` from `neovim-completion-manager`
- Updated `glv` git log alias to support interactivity in neovim
- Removed power menu from polybar
- Added prompt text to dmenu's like password menu, bookmarks menu, and
  application launcher
- Added critical border to dunst and changed timeouts

## [0.2.0] - 2018-08-01

### Added

- Moved to a branch based separation of configurations to make maintaining
  duplicate code between machines easier
- This CHANGELOG file to track changes in the project over time.
- Antigen plugin manager for zsh
- Created simple task manager with bash and zsh completion
- dmenu based bookmark manager bound to `alt-b`
- created etc folder to hold various root files not managed by stow
- blockips file to maintain an ip blacklist for my hosts file

### Changed

- Moved all pip packages that you can to pacman and aur equivalent packages for
  less messy updating through pacman
- Refreshed command line pinning script
- Moved systemd-root to new etc folder

## [0.1.1] - 2018-05-22

### Added

- Installation commands for `r` and `pip` packages

### Changed

- You now run make with `sudo` so you don't have to keep typing your password
  while the installation runs due to it timing out.
- Better checking of necessary folders or existing configuration files before
  syncing the git ones to the machine.

## 0.1.0 - 2018-05-21

### Added

- Makefile that installs all pacman and aur packages along with using GNU stow
  to put the necessary configuration files in place to get up and running with
  my Linux rice.

[Unreleased]: https://gitlab.com/mehalter/dotfiles/compare/v0.5.0...HEAD
[0.5.0]: https://gitlab.com/mehalter/dotfiles/compare/v0.4.0...v0.5.0
[0.4.0]: https://gitlab.com/mehalter/dotfiles/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/mehalter/dotfiles/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/mehalter/dotfiles/compare/v0.1.1...v0.2.0
[0.1.1]: https://gitlab.com/mehalter/dotfiles/compare/v0.1.0...v0.1.1
