
export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting

export QSYS_ROOTDIR="$HOME/intelFPGA_lite/17.1/quartus/sopc_builder/bin"

[ -f /usr/share/fzf/completion.bash ] && source /usr/share/fzf/completion.bash
[ -f /usr/share/fzf/key-bindings.bash ] && source /usr/share/fzf/key-bindings.bash
export FZF_DEFAULT_OPS="--extended"
export FZF_DEFAULT_COMMAND="fd --type f"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
