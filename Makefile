USER=micah
WALLPAPER=island.jpg

.PHONY: install update packages pacman yay arch aur r-packages pip-packages stow configs hosts sudo systemd root-config stow-pre stow-post

install: packages stow

update:
	# fully update git repository
	git pull
	git submodule update --init --recursive
	git submodule update --remote

packages: pacman arch yay aur r-packages pip-packages pamusb

pacman:
	# place pacman config
	cp etc/pacman.conf /etc/pacman.conf

yay: arch
	# install yay aur helper
	rm -rf /tmp/yay
	sudo -u $(USER) git clone https://aur.archlinux.org/yay.git /tmp/yay
	(cd /tmp/yay && sudo -u $(USER) makepkg -si)
	rm -rf /tmp/yay

pamusb:
	# install dependencies
	pacman -Sy --needed --noconfirm \
		libxml2 \
		python2-gobject \
		udisks2
	sudo -u $(USER) yay --noeditmenu --nodiffmenu --removemake --answerclean All -Sy \
		pmount-safe-removal
	# install pamusb
	rm -rf /tmp/pamusb
	sudo -u $(USER) git clone https://gitlab.com/mehalter/pam_usb.git /tmp/pamusb
	(cd /tmp/pamusb && make && make install)
	rm -rf /tmp/pamusb

arch: pacman
	# create home folders
	sudo -u $(USER) mkdir -p /home/$(USER)/Documents /home/$(USER)/Downloads /home/$(USER)/Music /home/$(USER)/Pictures /home/$(USER)/Videos
	pacman -Sy --needed --noconfirm \
		alsa-utils \
		aria2 \
		asciinema \
		acpi_call \
		biber \
		compton \
		cronie \
		ctags \
		cups \
		cups-pdf \
		dhclient \
		dmenu \
		dunst \
		exfat-utils \
		fd \
		firefox \
		fzf \
		gcc-fortran \
		gdb \
		gimp \
		gimp-plugin-gmic \
		git \
		gparted \
		gscan2pdf \
		i3-gaps \
		i3blocks \
		imagemagick \
		intel-ucode \
		jdk8-openjdk \
		jsoncpp \
		khal \
		khard \
		libreoffice-fresh \
		lxappearance \
		maim \
		mpv \
		msmtp \
		neomutt \
		neovim \
		network-manager-applet \
		networkmanager \
		newsboat \
		notmuch \
		notmuch-mutt \
		ntp \
		offlineimap \
		openssh \
		pandoc \
		pandoc-citeproc \
		pass \
		pavucontrol \
		pdfpc \
		powertop \
		pulseaudio \
		pulseaudio-alsa \
		pygmentize \
		python \
		python-i3-py \
		python-pip \
		qrencode \
		r \
		ranger \
		rsync \
		speedtest-cli \
		stow \
		sxiv \
		terminus-font \
		termite \
		texlive-most \
		tlp \
		tmux \
		openssh \
		unclutter \
		upower \
		vdirsyncer \
		vim \
		vlc \
		w3m \
		weechat \
		wget \
		xcape \
		xclip \
		xdotool \
		xorg-server \
		xorg-xinit \
		xorg-xinput \
		xorg-xrandr \
		xsettingsd \
		xwallpaper \
		youtube-dl \
		zathura \
		zathura-pdf-poppler \
		zsh
	# start tlp
	systemctl enable tlp.service
	systemctl enable tlp-sleep.service
	systemctl start tlp.service
	systemctl start tlp-sleep.service
	# start cups
	systemctl enable org.cups.cupsd.service
	systemctl enable cups-browsed.service
	systemctl start org.cups.cupsd.service
	systemctl start cups-browsed.service
	# start cronie
	systemctl enable cronie.service
	systemctl start cronie.service
	# start ntp
	systemctl enable ntpd.service
	systemctl start ntpd.service
	# start ssh
	systemctl enable sshd.service
	systemctl start sshd.service
	# change shell to zsh
	chsh -s /bin/zsh $(USER)

aur: yay
	# install all aur packages
	sudo -u $(USER) yay --noeditmenu --nodiffmenu --removemake --answerclean All -Sy \
		antibody \
		brother-dcp7065dn \
		brscan4 \
		cava \
		entr \
		gotop-bin \
		i3lock-color \
		light \
		magnet2torrent-git \
		ncpamixer \
		nerd-fonts-inconsolata \
		networkmanager-dmenu-git \
		qt5-webengine-widevine \
		qutebrowser-git \
		rtv \
		sc-im \
		skypeforlinux-stable-bin \
		tmuxinator \
		ttf-symbola \
		upass \
		urlscan \
		wpgtk-git

r-packages:
	Rscript -e "install.packages(c('bindr', 'DiagrammeR', 'knitr', 'reticulate', 'rmarkdown'), repos='https://cran.rstudio.com')"

pip-packages:
	sudo -u $(USER) pip install --user --upgrade pip
	sudo -u $(USER) pip install --user \
		gpymusic

stow: configs root-config stow-post

configs: update stow-pre
	# stow personal configs
	sudo -u $(USER) stow -S \
		bash \
		cava \
		compton \
		ctags \
		documents \
		dunst \
		fonts \
		gnupg \
		gtk \
		i3 \
		i3blocks \
		khal \
		khard \
		mpv \
		msmtp \
		mutt \
		ncpamixer \
		neofetch \
		neovim \
		newsboat \
		notmuch \
		offlineimap \
		pass \
		qutebrowser \
		ranger \
		readline \
		rtv \
		scim \
		scripts \
		ssh \
		systemd \
		termite \
		tmux \
		unclutter \
		urlview \
		vdirsyncer \
		weechat \
		wpgtk \
		xdgopen \
		xinit \
		zathura \
		zsh

hosts:
	# move block ips to host file
	sed -i '/^# Hosts contributed by Steven Black$$/,$$d' /etc/hosts
	cat etc/blockips >> /etc/hosts

sudo:
	# copy and start custom sudoers privs
	cp etc/sudoers.d/* /etc/sudoers.d/

systemd:
	# copy and start custom systemd services
	cp etc/systemd/system/* /etc/systemd/system/
	systemctl enable wakelock.service
	systemctl enable powertop.service
	systemctl start wakelock.service
	systemctl start powertop.service

root-config: hosts sudo systemd

stow-pre:
	# remove configs that will already exist
	-[ -L /home/$(USER)/.bashrc ] && mv /home/$(USER)/.bashrc /home/$(USER)/.bashrc.bak
	-[ -L /home/$(USER)/.config/cava ] && mv /home/$(USER)/.config/cava /home/$(USER)/.config/cava.bak
	-[ -L /home/$(USER)/.config/wpg ] && mv /home/$(USER)/.config/wpg /home/$(USER)/.config/wpg.bak
	# create folder that might not be created yet for configs
	sudo -u $(USER) mkdir -p /home/$(USER)/.local/share/applications /home/$(USER)/.config/wpg /home/$(USER)/.config/systemd/user/default.target.wants

stow-post:
	# shortcuts initialization
	sudo -u $(USER) /home/$(USER)/.config/Scripts/shortcuts.sh
	# neovim vim-plug initialization
	sudo -u $(USER) nvim +'PlugInstall --sync' +qa
	# set wallpaper and run wpgtk
	sudo -u $(USER) cp /home/$(USER)/Pictures/wallpapers/$(WALLPAPER) /home/$(USER)/.config/wall
	sudo -u $(USER) wpg-install -g -i
	sudo -u $(USER) wpg -s island.jpg
