# dotfiles
My dotfiles for my Linux rice, managed using stow.

![Desktop Preview](preview.png)

Check out the [wiki](https://gitlab.com/mehalter/dotfiles/wikis/home) for
documentation on the custom bindings that have been added to these applications

## Installation

I have included a Makefile for this project that will take a fresh Arch Linux
installation and install all the packages necessary and configure them to run my
Linux rice.

### Prerequisites

+ Arch installed with `base` and `base-devel`
+ A network connection
+ A regular, non-root user set up with `sudo` permissions
+ Packages
    + `git`

### Installing

```console
~$ git clone https://gitlab.com/mehalter/dotfiles.git ~/dotfiles
~$ cd ~/dotfiles
~$ sudo make install
~$ reboot
```

## Applications

### Window Management

- [i3-gaps](https://github.com/Airblader/i3)
- [i3lock-color](https://github.com/PandorasFox/i3lock-color)
- [polybar](https://github.com/jaagr/polybar)
- [j4-dmenu-desktop](https://github.com/enkore/j4-dmenu-desktop)

### Command Line Applications

- [cava](https://github.com/karlstav/cava)
- [gpymusic](https://github.com/christopher-dG/gpymusic)
- [htop](https://github.com/hishamhm/htop)
- [khal](https://github.com/pimutils/khal)
- [khard](https://github.com/scheibler/khard)
- [mps-youtube](https://github.com/mps-youtube/mps-youtube)
- [neomutt](https://github.com/neomutt/neomutt)
- [neovim](https://github.com/neovim/neovim)
- [newsboat](https://github.com/newsboat/newsboat)
- [notmuch](https://notmuchmail.org/)
- [qutebrowser](https://github.com/qutebrowser/qutebrowser)
- [ranger](https://github.com/ranger/ranger)
- [rtv](https://github.com/michael-lazar/rtv)
- [sc-im](https://github.com/andmarti1424/sc-im)
- [tmux](https://github.com/tmux/tmux)
- [tmuxinator](https://github.com/tmuxinator/tmuxinator)
- [weechat](https://github.com/weechat/weechat)
- Many more...

## Look and Feel

### Themes

- wal gtk theme
- [numix icon theme](https://github.com/numixproject/numix-icon-theme)

### Fonts

- [Terminus](http://terminus-font.sourceforge.net/)
- [siji](https://github.com/stark/siji)
