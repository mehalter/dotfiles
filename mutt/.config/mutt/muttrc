source $HOME/.config/mutt/gmailrc
source $HOME/.config/mutt/muttcol
source $HOME/.config/mutt/aliases

set editor = $EDITOR
set timeout = "5"
set wait_key = no
set mail_check = "10"
set mailcap_path = $HOME/.config/mutt/mailcap
set date_format="%m/%d %I:%M"
set index_format="%?M?+& ?%2M %2C %Z %d %-15.15F %s (%-4.4c)"
set signature = $HOME/.config/mutt/signature
set sendmail_wait = -1

# Threads
set sort = 'threads'
set sort_aux = 'reverse-last-date-received'
set strict_threads="yes"
bind index <Space> collapse-thread
bind index - collapse-thread
bind index _ collapse-all
exec collapse-all

auto_view text/html
alternative_order text/plain text/enriched text/html

mono bold bold
mono underline underline
mono indicator reverse
color index yellow default '.*' #normal email
color index green  default "~v~(!~N)~(!~F)" #collapsed threads
color index red  default "~T" #tagged
color index_author red default '.*'
color index_number blue default
color index_subject cyan default '.s'
color index_size green default
color normal default default
color body brightred default [\-\.+_a-zA-Z0-9]+@[\-\.a-zA-Z0-9]+
color body brightblue default (https?|ftp)://[\-\.,/%~_:?&=\#a-zA-Z0-9]+
color indicator blue default

macro index q "<shell-escape>pkill -SIGRTMIN+13 i3blocks<enter><quit>" "exit and refresh polybar"
bind editor <space> noop
bind index G last-entry
bind index gg first-entry
bind index d half-down
bind index u half-up
bind index D delete-message
bind index U undelete-message
bind index F search
bind index R group-reply

# address book settings
set query_command="khard email --parsable %s"
bind editor <Tab> complete-query
bind editor ^T complete
bind index,pager a noop
macro index,pager ac "<pipe-message>khard add-email<return>" "Add sender to Khard"
bind index,pager s noop
macro index,pager sc "<shell-escape>vdirsyncer sync gaddr<return>" "sync contacts"

macro index Z "<shell-escape>offlineimap<enter>" "sync all mail"

macro index,pager \cu "<pipe-message> urlscan<Enter>" "call urlscan to extract URLs out of a message"
macro attach,compose \cu "<pipe-entry> urlscan<Enter>" "call urlscan to extract URLs out of a message"

# notmuch search
macro index <F8> \
"<enter-command>set my_old_pipe_decode=\$pipe_decode my_old_wait_key=\$wait_key nopipe_decode nowait_key<enter>\
<shell-escape>notmuch-mutt -r --prompt search<enter>\
<change-folder-readonly>`echo ${XDG_CACHE_HOME:-$HOME/.cache}/notmuch/mutt/results`<enter>\
<enter-command>set pipe_decode=\$my_old_pipe_decode wait_key=\$my_old_wait_key<enter>" \
"notmuch: search mail"

macro index <F9> \
"<enter-command>set my_old_pipe_decode=\$pipe_decode my_old_wait_key=\$wait_key nopipe_decode nowait_key<enter>\
<pipe-message>notmuch-mutt -r thread<enter>\
<change-folder-readonly>`echo ${XDG_CACHE_HOME:-$HOME/.cache}/notmuch/mutt/results`<enter>\
<enter-command>set pipe_decode=\$my_old_pipe_decode wait_key=\$my_old_wait_key<enter>" \
"notmuch: reconstruct thread"
